@tool
extends Node

@onready var lstName: ItemList = $Name/List
@onready var lstFile: ItemList = $File/List

func _on_name_list_item_activated(index: int) -> void:
	_on_file_list_item_activated(index)

func _on_name_list_item_clicked(
	index: int,
	at_position: Vector2,
	mouse_button_index: int
) -> void:
	lstFile.select(index, true)

func _on_file_list_item_activated(index: int) -> void:
	OS.alert("SUCCESSFUL")
	print_debug("OOGA BOOGA")

func _on_file_list_item_clicked(
	index: int,
	at_position: Vector2,
	mouse_button_index: int
) -> void:
	lstName.select(index, true)
