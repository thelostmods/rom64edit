@tool
class_name Rom64Edit
extends EditorPlugin

const Interface = preload("res://addons/rom64edit/scenes/Interface.tscn")
var interface: Control

func _build() -> bool: return false # No need for play environment!
func _enter_tree() -> void: _enable_plugin()
func _exit_tree() -> void: _disable_plugin()

func _enable_plugin() -> void:
	Rom64Edit_Settings.initialize()
	
	if interface: return
	interface = Interface.instantiate()
	add_control_to_dock(EditorPlugin.DOCK_SLOT_LEFT_UL, interface)

func _disable_plugin() -> void:
	if not interface: return
	remove_control_from_docks(interface)
	interface.queue_free()

func _get_plugin_icon() -> Texture2D:
	# You can use a custom icon:
	#return preload("res://addons/my_plugin/my_plugin_icon.svg")
	# Or use a built-in icon:
	return get_editor_interface().get_base_control().get_icon("Node", "EditorIcons")

func _make_visible(visible: bool) -> void:
	if interface: interface.visible = visible
