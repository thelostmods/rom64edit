@tool
class_name Rom64Edit_Settings

const dir_root = "rom64edit/"
const dir_conf = dir_root + "config/"

static func initialize() -> void:
	# project defaults
	set_field(
		"application/config/name",
		"Rom64Edit Studio"
	); set_field(
		"application/config/description",
		"An [open source, all-in-one, expandable] N64 rom editor."
	); set_field(
		"application/config/icon",
		"res://addons/rom64edit/assets/images/icon.png"
	); set_field(
		"application/boot_splash/show_image",
		true
	); set_field(
		"application/boot_splash/image",
		"res://addons/rom64edit/assets/images/splash.png"
	); set_field(
		"application/boot_splash/fullsize",
		false
	); set_field(
		"application/boot_splash/bg_color",
		Color(0.12, 0.12, 0.12, 1.0)
	)
	
	# rom64edit config
	var field: String
	
	field = dir_conf + "current_project"
	add_field(
		field,
		"",
		{
			"name": field,
			"type": TYPE_STRING,
			"hint_string": "This property defines the last used project folder.\r\n" +\
							"default:[] | example:[my_romhack -> 'res://projects/my_romhack']"
		}
	); ProjectSettings.set_order(field, 0)

static func add_field(field: String, value: Variant, create: Dictionary = {}) -> void:
	if not ProjectSettings.has_setting(field):
		set_field(field, value, create)

static func get_field(field: String) -> String:
	if not ProjectSettings.has_setting(field): initialize()
	if not ProjectSettings.has_setting(field): return ""
	return ProjectSettings.get_setting(field)

static func set_field(field: String, value: Variant, create: Dictionary = {}) -> void:
	if ProjectSettings.has_setting(field):
		ProjectSettings.set_setting(field, value)
	else:
		if create == {}: return
		
		ProjectSettings.set_setting(field, value)
		ProjectSettings.set_initial_value(field, value)
		ProjectSettings.add_property_info(create)

static func get_current_project_full() -> String:
	return "res://projects/" + get_current_project()

static func get_current_project() -> String:
	var directory : String = get_field(dir_conf + "current_project")
	Helper.make_directory("res://projects/" + directory)
	
	if directory.right(0) != "/":
		directory += "/"
		
	return directory

static func set_current_project(path: String) -> void:
	set_field(dir_conf + "current_project", path)
	Helper.make_directory("res://projects/" + path)
