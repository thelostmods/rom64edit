@tool
extends Node

var _plugins: Dictionary

#Todo: Get rid of this (Need a networked plugin list, I only want r64e to include a plugin interface for downloading new and updating existing plugins)
func _find_plugins() -> void:
	_plugins.clear()
	_find_plugins_recursively("res://addons/")
	
	var lstPlugins : ItemList = $Align/Plugins/Align/Scroll/List
	for plugin in _plugins:
		lstPlugins.add_item(plugin)

func _find_plugins_recursively(path: String) -> void:
	var dir := Directory.new()
	
	if dir.open(path) != OK: return
	
	dir.list_dir_begin()
	
	var files: Array[String]
	
	while true:
		var file : String = dir.get_next()
		
		if file == "": break
		elif dir.current_is_dir():
			_find_plugins_recursively(path + "/" + file)
		elif file == "plugin.cfg":
			files.append(path + "/" + file)
	
	dir.list_dir_end()
	
	for file in files:
		var fi := File.new()
		if fi.open(file, File.READ) != OK: continue
		
		while not fi.eof_reached():
			var line : String = fi.get_line()
			if not line.begins_with("name="): continue
			
			var name : String = line.substr(5, line.length())
			if name == '"Rom64Edit"': continue
			_plugins[name] = file
			break
		
		fi.close()

func _ready():
	_find_plugins()
