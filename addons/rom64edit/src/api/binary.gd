@tool
class_name Binary

static func get_from_bytes(value: int, offset: int, size: int) -> int:
	if offset + size > 8:
		push_error("Requested byte-range was out of range!")
		return 0
	
	offset *= 8; size *= 8
	return (value >> (64 - offset - size)) & ((1 << size) - 1)

static func get_from_bits(value: int, offset: int, size: int) -> int:
	if offset + size > 64:
		push_error("Requested bit-range was out of range!")
		return 0
	
	return (value >> (64 - offset - size)) & ((1 << size) - 1)
