@tool
class_name Hardware

################################
# Controller inputs to hex codes
################################
enum Controller
{
	CRight = 0x0001,
	CLeft  = 0x0002,
	CDown  = 0x0004,
	CUp    = 0x0008,
	R      = 0x0010,
	L      = 0x0020,
	DRight = 0x0100,
	DLeft  = 0x0200,
	DDown  = 0x0400,
	DUp    = 0x0800,
	Start  = 0x1000,
	Z      = 0x2000,
	B      = 0x4000,
	A      = 0x8000
}

#####################
## Header Offsets
###########

const header_pi_bsb_dom1_lat_reg := 0x00 # 1  (u)byte
const header_pi_bsd_dom1_pgs_reg := 0x01 # 1  (u)byte
const header_pi_bsd_dom1_pwd_reg := 0x02 # 1  (u)byte
const header_pi_bsb_dom1_pgs_reg := 0x03 # 1  (u)byte
const header_clock_rate          := 0x04 # 1  (u)int
const header_program_counter     := 0x08 # 1  (u)int
const header_release_pointer     := 0x0C # 1  (u)int
const header_crc1                := 0x10 # 1  (u)int
const header_crc2                := 0x14 # 1  (u)int
const header_unk1                := 0x18 # 2  (u)int
const header_name                := 0x20 # 20 char
const header_unk2                := 0x34 # 1  (u)int
const header_media_format        := 0x38 # 1  (u)int
const header_game_id             := 0x3C # 1  (u)short
const header_country_code        := 0x3E # 1  char
const header_revision            := 0x3F # 1  (u)byte
