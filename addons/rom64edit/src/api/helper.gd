@tool
class_name Helper

##############################################
# Returns a C-Style string from raw array data
##############################################
static func c_string(
	arr: PackedByteArray,
	offset: int,
	maxLen: int = -1
) -> String:
	var ret     : String
	var arrSize : int = arr.size()
	
	while maxLen != 0:
		if arr[offset] == 0: break
		if offset > arrSize: break
		if maxLen == 0: break
		
		ret += char(arr[offset])
		offset += 1
		if maxLen > 0: maxLen -= 1
	
	return ret

##################################
# Returns true if directory exists
##################################
static func directory_exists(path: String) -> bool:
	return Directory.new().dir_exists(path)

#############################################
# Creates directory only if it does not exist
#############################################
static func make_directory(path: String) -> void:
	var directory := Directory.new()
	if not directory.file_exists(path):
		directory.make_dir_recursive(path)

##########################################################################
# Returns an array of file paths with their full path information included
##########################################################################
static func get_files(path: String, ext: String) -> Array[String]:
	var dir := Directory.new()
	
	if not dir.dir_exists(path):
		return []
	
	if dir.open(path) != OK:
		return []
	
	dir.list_dir_begin()
	
	var files: Array[String]
	
	while true:
		var file : String = dir.get_next()
		
		if file == "": break
		elif not file.begins_with(".") and file.ends_with("." + ext):
			files.append(file)
	
	dir.list_dir_end()
	
	return files

###############################################################
# Returns a list of detected rom files (by file extension only)
###############################################################
static func get_roms(path: String) -> Array[String]:
	var rom_files: Array[String]
	
	for s in get_files(path, "z64"): rom_files.append(s)
	for s in get_files(path, "n64"): rom_files.append(s)
	for s in get_files(path, "v64"): rom_files.append(s)
	
	return rom_files

######################
# Returns a rom binary
######################
static func get_rom(path: String) -> PackedByteArray:
	var file := File.new()
	
	# Safety check so godot shuts up
	if not file.file_exists(path):
		return PackedByteArray()
	
	var rom_data: PackedByteArray
	var rom_size: int
	file.open(path, File.READ)
	rom_size = file.get_length()
	rom_data = file.get_buffer(rom_size)
	file.close()
	
	if rom_size % 4 != 0:
		return PackedByteArray()
	
	match BigEndian.arr_u32(rom_data, 0):
		0x80371240: # z64
			pass
			
		0x40123780: # n64
			var tmp_rom: PackedByteArray
			for i in range(0, rom_size, 4):
				tmp_rom.append(rom_data[i + 3])
				tmp_rom.append(rom_data[i + 2])
				tmp_rom.append(rom_data[i + 1])
				tmp_rom.append(rom_data[i + 0])
			rom_data = tmp_rom
			
		0x37804012: # v64
			var tmp_rom: PackedByteArray
			for i in range(0, rom_size, 2):
				tmp_rom.append(rom_data[i + 1])
				tmp_rom.append(rom_data[i + 0])
			rom_data = tmp_rom
			
		_: return PackedByteArray()
	
	return rom_data
