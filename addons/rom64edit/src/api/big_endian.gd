@tool
class_name BigEndian

########################################################
# Converts a 16-bit unsigned integer into a signed value
########################################################
static func to_s16(value: int) -> int:
	if value >= 0x8000:
		return -(0x10000 - value)
	return value

########################################################
# Converts a 32-bit unsigned integer into a signed value
########################################################
static func to_s32(value: int) -> int:
	if value >= 0x80000000:
		return -(0x100000000 - value)
	return value

##########################################################
# Returns a 16-bit integer value from the specified offset
##########################################################
static func arr_s16(arr: PackedByteArray, offset: int) -> int:
	return to_s16(arr_u16(arr, offset))

##########################################################
# Returns a 32-bit integer value from the specified offset
##########################################################
static func arr_s32(arr: PackedByteArray, offset: int) -> int:
	return to_s32(arr_u32(arr, offset))

###################################################################
# Returns a 16-bit unsigned integer value from the specified offset
###################################################################
static func arr_u16(arr: PackedByteArray, offset: int) -> int:
	if offset > arr.size():
		print_debug("Offset was out of range!")
		return 0
		
	return  (arr[offset+0] << 8) |\
			(arr[offset+1])

###################################################################
# Returns a 32-bit unsigned integer value from the specified offset
###################################################################
static func arr_u32(arr: PackedByteArray, offset: int) -> int:
	if offset > arr.size():
		print_debug("Offset was out of range!")
		return 0
		
	return  (arr[offset+0] << 24) |\
			(arr[offset+1] << 16) |\
			(arr[offset+2] << 8) |\
			(arr[offset+3])


##########################################################
# Returns a 64-bit integer value from the specified offset
##########################################################
static func arr_64(arr: PackedByteArray, offset: int) -> int:
	if offset + 8 > arr.size():
		print_debug("Offset was out of range!")
		return 0
		
	return  (arr[offset+0] << 56) |\
			(arr[offset+1] << 48) |\
			(arr[offset+2] << 40) |\
			(arr[offset+3] << 32) |\
			(arr[offset+4] << 24) |\
			(arr[offset+5] << 16) |\
			(arr[offset+6] << 8) |\
			(arr[offset+7])
