@tool
class_name N64Vertex

var position : Vector3
var uv       : Vector2
var color    : Color

func _init(
	position : Vector3 = Vector3.ZERO,
	uv       : Vector2 = Vector2.ZERO,
	color    : Color   = Color.WHITE
) -> void:
	self.position = position
	self.uv       = uv
	self.color    = color
