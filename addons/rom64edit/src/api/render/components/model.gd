@tool
class_name N64Model

var textures : TextureCache

var _display_list : DisplayList
var _vertex_list  : VertexList

func _init(
	display_list : DisplayList,
	vertex_list  : VertexList,
	textures     : TextureCache = TextureCache.new(0)
) -> void:
	_display_list = display_list
	_vertex_list  = vertex_list
	self.textures = textures

#######################
# Display List Handlers
#######################

func display_list_count() -> int:
	return _display_list.count()

func get_display_command_name(index: int) -> String:
	return _display_list.get_cmd_name(index)

func get_display_command_byte(index: int, offset: int) -> int:
	return _display_list.get_cmd_byte(index, offset)

func get_display_command_bytes(index: int, offset: int, size: int) -> int:
	return _display_list.get_cmd_bytes(index, offset, size)

func get_display_command_bits(index: int, offset: int, size: int) -> int:
	return _display_list.get_cmd_bits(index, offset, size)

######################
# Vertex List Handlers
######################

func vertex_list_count() -> int:
	return _vertex_list.count()

func get_vertex(index: int) -> N64Vertex:
	return _vertex_list.get_vertex(index)

###############
# Image Formats
###############

func get_image_ci4(texture: int, palette: Array[Color]) -> Image:
	var t_info : TextureInfo = textures.info[texture]
	var offset : int         = t_info.offset + palette.size()
	
	var img := Image.new()
	img.create(t_info.width, t_info.height, false, Image.FORMAT_RGBA8)
	
	for y in t_info.height:
		for x in range(0, t_info.width, 2):
			var bits : int = textures.data[offset]
			img.set_pixel(x + 0, y, palette[bits >> 4])
			img.set_pixel(x + 1, y, palette[bits & 0xF])
			offset += 1
	
	return img

func get_image_ci8(texture: int, palette: Array[Color]) -> Image:
	var t_info : TextureInfo = textures.info[texture]
	var offset : int         = t_info.offset + palette.size()
	
	var img := Image.new()
	img.create(t_info.width, t_info.height, false, Image.FORMAT_RGBA8)
	
	for y in t_info.height:
		for x in t_info.width:
			img.set_pixel(x + 0, y, palette[textures.data[offset]])
			offset += 1
	
	return img

func get_image_ia4(texture: int, palette: Array[Color]) -> Image:
	var t_info : TextureInfo = textures.info[texture]
	var offset : int         = t_info.offset + palette.size()
	
	var img := Image.new()
	img.create(t_info.width, t_info.height, false, Image.FORMAT_RGBA8)
	
	for y in t_info.height:
		for x in range(0, t_info.width, 2):
			var bits : int = textures.data[offset]
			var c1   : int = (bits >> 4) * 17
			var c2   : int = (bits & 0xF) * 17
			img.set_pixel(x + 0, y, Color(c1, c1, c1, c1))
			img.set_pixel(x + 1, y, Color(c2, c2, c2, c2))
			offset += 1
	
	return img

func get_image_ia8(texture: int, palette: Array[Color]) -> Image:
	var t_info : TextureInfo = textures.info[texture]
	var offset : int         = t_info.offset + palette.size()
	
	var img := Image.new()
	img.create(t_info.width, t_info.height, false, Image.FORMAT_RGBA8)
	
	for y in t_info.height:
		for x in t_info.width:
			var bits : int = textures.data[offset]
			var c1   : int = (bits >> 4) * 17
			var c2   : int = (bits & 0xF) * 17
			img.set_pixel(x + 0, y, Color(c1, c1, c1, c2))
			offset += 1
	
	return img
