@tool
class_name DisplayList

class DisplayCommand:
	var name : String
	var data : int
	
	func _init(name: String, data: int) -> void:
		self.name = name
		self.data = data

var commands : Array[DisplayCommand]

func _init(data : PackedByteArray) -> void:
	commands.resize(data.size() / 0x8)
	
	for i in count():
		var offset : int = i * 0x8
		commands[i] = DisplayCommand.new(
			"%02X" % data[offset],
			BigEndian.arr_64(data, offset) & 0xFFFFFFFFFFFFFF
		)

func count() -> int:
	return commands.size()

func get_cmd_name(index: int) -> String:
	if index > count():
		push_error("Provided index was out of Display List command range!")
		return "00"
	
	return commands[index].name

func get_cmd(index: int) -> int:
	if index > count():
		push_error("Provided index was out of Display List command range!")
		return 0
	
	return commands[index].data

func get_cmd_byte(index: int, offset: int) -> int:
	if offset > 7:
		push_error("Requested Display List (byte) was out of range!")
		return 0
	
	return (get_cmd(index) >> (56 - offset * 0x8)) & 0xFF

func get_cmd_bytes(index: int, offset: int, size: int) -> int:
	return Binary.get_from_bytes(get_cmd(index), offset, size)

func get_cmd_bits(index: int, offset: int, size: int) -> int:
	return Binary.get_from_bits(get_cmd(index), offset, size)
