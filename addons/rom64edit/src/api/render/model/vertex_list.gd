@tool
class_name VertexList

var vertices : Array[N64Vertex]

func _init(data : PackedByteArray) -> void:
	vertices.resize(data.size() / 0x10)
	
	for i in count():
		var offset : int = i * 0x10
		vertices[i] = N64Vertex.new(
			Vector3( # position
				BigEndian.arr_s16(data, offset + 0x0),
				BigEndian.arr_s16(data, offset + 0x2),
				BigEndian.arr_s16(data, offset + 0x4)
			),
			Vector2( # uv
				BigEndian.arr_s16(data, offset + 0x6),
				BigEndian.arr_s16(data, offset + 0x8)
			),
			Color(   # color
				float(data[offset + 0xC]) / 0xFF,
				float(data[offset + 0xD]) / 0xFF,
				float(data[offset + 0xE]) / 0xFF,
				float(data[offset + 0xF]) / 0xFF
			)
		)

func count() -> int:
	return vertices.size()

func get_vertex(index: int) -> N64Vertex:
	if index > count():
		push_error("Provided index was out of Vertex List range!")
		return N64Vertex.new()
	
	return vertices[index]
