@tool
class_name Fast3D
extends RenderEngine

######################
# Embedded Enumerators
######################

enum GeometryMode {
	SHADE              = 0x00000004, # Enables calculation of vertex color for a triangle.
	LIGHTING           = 0x00020000, # Enables lighting calculations.
	SHADING_SMOOTH     = 0x00000200, # Enables Gouraud shading. When this is not enabled, flat shading is used for the triangle, based on the color of one vertex (see gSP1Triangle). G_SHADE must be enabled to calculate vertex color.
	ZBUFFER            = 0x00000001, # Enables Z buffer calculations. Other Z buffer-related parameters for the frame buffer must also be set.
	TEXTURE_GEN        = 0x00040000, # Enables automatic generation of the texture's s,t coordinates. Spherical mapping based on the normal vector is used.
	TEXTURE_GEN_LINEAR = 0x00080000, # Enables automatic generation of the texture's s,t coordinates.
	CULL_FRONT         = 0x00001000, # Pointer to display list.
	CULL_BACK          = 0x00002000, # Enables front-face culling. Note: This is not supported on F3DEX Rej/F3DLP Rej.
	CULL_BOTH          = 0x00003000, # Enables back-face culling. Note: This is not supported on F3DEX Rej/F3DLP Rej.
	FOG                = 0x00010000, # Enables generation of vertex alpha coordinate fog parameters. Note: Enabling this means you cannot use vertex alpha.
	CLIPPING           = 0x00800000  # Enables clipping. This mode is enabled in the initial state. When disabled, clipping is not performed. Note: This is only supported on F3DLX and F3DLX NoN. 
}

#####################
# Command Initializer
#####################

func _init_commands():
	super._init_commands()
	
	var commands : Dictionary = {
		"00" = "SPNOOP",
		"01" = "MTX",
		"03" = "MOVEMEM",
		"04" = "VTX",
		"06" = "DL",
		"BF" = "TRI1",
		"BE" = "CULLDL",
		"BD" = "POPMTX",
		"BC" = "MOVEWORD",
		"BB" = "TEXTURE",
		"BA" = "SETOTHERMODE_H",
		"B9" = "SETOTHERMODE_L",
		"B8" = "ENDDL",
		"B7" = "SETGEOMETRYMODE",
		"B6" = "CLEARGEOMETRYMODE",
		"B4" = "RDPHALF_1",
		"B3" = "RDPHALF_2",
		"B2" = "RDPHALF_CONT",
		"C0" = "NOOP",
		"EF" = "RDPSETOTHERMODE",
		"EE" = "SETPRIMDEPTH",
		"ED" = "SETSCISSOR",
		"EC" = "SETCONVERT",
		"EB" = "SETKEYR",
		"EA" = "SETKEYGB",
		"E9" = "RDOFULLSYNC",
		"E8" = "RDPTILESYNC",
		"E7" = "RDPPIPESYNC",
		"E6" = "RDPLOADSYNC",
		"E5" = "TEXRECTFLIP",
		"FF" = "SETCIMG",
		"FE" = "SETZIMG",
		"FD" = "SETTIMG",
		"FC" = "SETCOMBINE",
		"FB" = "SETENVCOLOR",
		"FA" = "SETPRIMCOLOR",
		"F9" = "SETBLENDCOLOR",
		"F8" = "SETFOGCOLOR",
		"F7" = "SETFILLCOLOR",
		"F6" = "FILLRECT",
		"F5" = "SETTILE",
		"F4" = "LOADTILE",
		"F3" = "LOADBLOCK",
		"F2" = "SETTILESIZE",
		"F0" = "LOADTLUT"
	}
	
	for cmd in commands:
		_commands[cmd] = commands[cmd]

##################
# Command Handlers
##################

func HANDLE_SPNOOP(model: N64Model, index: int) -> void: # 00
	pass # does nothing

func HANDLE_MTX(model: N64Model, index: int) -> void: # 01
	pass

func HANDLE_MOVEMEM(model: N64Model, index: int) -> void: # 03
	pass

func HANDLE_VTX(model: N64Model, index: int) -> void: # 04
	pass

func HANDLE_DL(model: N64Model, index: int) -> void: # 06
	pass

func HANDLE_TRI1(model: N64Model, index: int) -> void: # BF
	var v1 : int = model.get_display_command_byte(index, 5) / 0x2
	var v2 : int = model.get_display_command_byte(index, 6) / 0x2
	var v3 : int = model.get_display_command_byte(index, 7) / 0x2
	_mesh_renderer.draw_array(
		PackedVector3Array([ # vertices
			_vertex_list[v1].position * n64_vertex_scale,
			_vertex_list[v3].position * n64_vertex_scale,
			_vertex_list[v2].position * n64_vertex_scale
		]),
		PackedVector2Array([ # uvs
			_vertex_list[v1].uv,
			_vertex_list[v3].uv,
			_vertex_list[v2].uv
		]),
		PackedColorArray([   # colors
			_vertex_list[v1].color,
			_vertex_list[v3].color,
			_vertex_list[v2].color,
		])
	)

func HANDLE_CULLDL(model: N64Model, index: int) -> void: # BE
	pass

func HANDLE_POPMTX(model: N64Model, index: int) -> void: # BD
	pass

func HANDLE_MOVEWORD(model: N64Model, index: int) -> void: # BC
	pass

# TODO: Redo this from documentation
func HANDLE_TEXTURE(model: N64Model, index: int) -> void: # BB
	# exit in case textures aren't provided
	if model.textures.count() < 1: return
	
	# TODO: Revamp this using bits command
	var bits  : int = model.get_display_command_bytes(index, 0x2, 2)
	var s     : int = model.get_display_command_bytes(index, 0x4, 2)
	var t     : int = model.get_display_command_bytes(index, 0x6, 2)
	var tile  : int = (bits >> 08) & 7
	var level : int = (bits >> 11) & 7
	var on    : int = bits & 0xFE
	
	if tile > 1: return
	
	#gMatState.tile[tile].on = on;
	
	if not on: return
	
	#gMatState.tile[tile].level = level;
	_st_scale = Vector2(
		s * (1.0 / 0xFFFF),
		t * (1.0 / 0xFFFF)
	)

func HANDLE_SETOTHERMODE_H(model: N64Model, index: int) -> void: # BA
	pass

func HANDLE_SETOTHERMODE_L(model: N64Model, index: int) -> void: # B9
	pass

func HANDLE_ENDDL(model: N64Model, index: int) -> void: # B8
	pass

func HANDLE_SETGEOMETRYMODE(model: N64Model, index: int) -> void: # B7
	_geometry_mode = model.get_display_command_bytes(index, 0x4, 4)

func HANDLE_CLEARGEOMETRYMODE(model: N64Model, index: int) -> void: # B6
	_geometry_mode = 0

func HANDLE_RDPHALF_1(model: N64Model, index: int) -> void: # B4
	pass

func HANDLE_RDPHALF_2(model: N64Model, index: int) -> void: # B3
	pass

func HANDLE_RDPHALF_CONT(model: N64Model, index: int) -> void: # B2
	pass

func HANDLE_NOOP(model: N64Model, index: int) -> void: # C0
	pass # does nothing

func HANDLE_RDPSETOTHERMODE(model: N64Model, index: int) -> void: # EF
	pass

func HANDLE_SETPRIMDEPTH(model: N64Model, index: int) -> void: # EE
	pass

func HANDLE_SETSCISSOR(model: N64Model, index: int) -> void: # ED
	pass

func HANDLE_SETCONVERT(model: N64Model, index: int) -> void: # EC
	pass

func HANDLE_SETKEYR(model: N64Model, index: int) -> void: # EB
	pass

func HANDLE_SETKEYGB(model: N64Model, index: int) -> void: # EA
	pass

func HANDLE_RDOFULLSYNC(model: N64Model, index: int) -> void: # E9
	pass

func HANDLE_RDPTILESYNC(model: N64Model, index: int) -> void: # E8
	pass

func HANDLE_RDPPIPESYNC(model: N64Model, index: int) -> void: # E7
	pass

func HANDLE_RDPLOADSYNC(model: N64Model, index: int) -> void: # E6
	pass

func HANDLE_TEXRECTFLIP(model: N64Model, index: int) -> void: # E5
	pass

func HANDLE_SETCIMG(model: N64Model, index: int) -> void: # FF
	pass

func HANDLE_SETZIMG(model: N64Model, index: int) -> void: # FE
	pass

# TODO: Redo this from documentation
func HANDLE_SETTIMG(model: N64Model, index: int) -> void: # FD
	# exit in case textures aren't provided
	if model.textures.count() < 1: return
	
	var offset : int = model.get_display_command_bytes(index, 5, 3)
	_cur_texture = model.textures.find_index(offset)
	
	var next_cmd : String = "%02X" % model.get_display_command_byte(index + 2, 0)
	if (_commands[next_cmd] == "LOADTLUT"):
		return
	
	_new_texture = true

func HANDLE_SETCOMBINE(model: N64Model, index: int) -> void: # FC
	pass

func HANDLE_SETENVCOLOR(model: N64Model, index: int) -> void: # FB
	pass

func HANDLE_SETPRIMCOLOR(model: N64Model, index: int) -> void: # FA
	pass

func HANDLE_SETBLENDCOLOR(model: N64Model, index: int) -> void: # F9
	pass

func HANDLE_SETFOGCOLOR(model: N64Model, index: int) -> void: # F8
	pass

func HANDLE_SETFILLCOLOR(model: N64Model, index: int) -> void: # F7
	pass

func HANDLE_FILLRECT(model: N64Model, index: int) -> void: # F6
	pass

func HANDLE_SETTILE(model: N64Model, index: int) -> void: # F5
	pass

func HANDLE_LOADTILE(model: N64Model, index: int) -> void: # F4
	pass

func HANDLE_LOADBLOCK(model: N64Model, index: int) -> void: # F3
	pass

# TODO: Redo this from documentation
func HANDLE_SETTILESIZE(model: N64Model, index: int) -> void: # F2
	# exit in case textures aren't provided
	if model.textures.count() < 1: return
	
	_tex_scale = Vector2(
		_st_scale.x / (model.textures.info[_cur_texture].width  << 5),
		_st_scale.y / (model.textures.info[_cur_texture].height << 5)
	)

# TODO: Redo this from documentation
func HANDLE_LOADTLUT(model: N64Model, index: int) -> void: # F0
	# exit in case textures aren't provided
	if model.textures.count() < 1: return
	
	var palette_size : int = model.get_display_command_bits(index, 0x28, 0xA) * 2 + 2
	_palette.resize(palette_size)
	
	var t_info : TextureInfo = model.textures.info[_cur_texture]
	
	var offset : int = 0
	for i in range(0, palette_size, 2):
		var p_code : int = BigEndian.arr_u16(model.textures.data, t_info.offset + i)
		_palette[offset] = Color(
			(((p_code & 0xF800) >> 11) * 008) / 255.0,
			(((p_code & 0x07C0) >> 06) * 008) / 255.0,
			(((p_code & 0x003E) >> 01) * 008) / 255.0,
			(((p_code & 0x0001) >> 00) * 255) / 255.0
		)
		
		offset += 1
	
	# print Texture
	var img : Image = model.get_image_ci4(_cur_texture, _palette)
	img.save_png("res://projects/banjo_kazooie/textures/%s.png" % _cur_texture)
