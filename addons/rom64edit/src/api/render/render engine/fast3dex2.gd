@tool
class_name Fast3DEX2
extends Fast3DEX

#####################
# Command Initializer
#####################

func _init_commands():
	super._init_commands()
	
	var commands : Dictionary = {
		"00" = "NOOP",
		"01" = "VTX",
		"02" = "MODIFYVTX",
		"03" = "CULLDL",
		"04" = "BRANCH_Z",
		"05" = "TRI1",
		"06" = "TRI2",
		"07" = "QUAD",
		"DF" = "ENDDL",
		"DE" = "DL",
		"DD" = "LOAD_UCODE",
		"DC" = "MOVEMEM",
		"DB" = "MOVEWORD",
		"DA" = "MTX",
		"D9" = "GEOMETRYMODE",
		"D8" = "POPMTX",
		"D7" = "TEXTURE",
		"D6" = "DMA_IO",
		"D5" = "SPECIAL_1",
		"D4" = "SPECIAL_2",
		"D3" = "SPECIAL_3",
		"E3" = "SETOTHERMODE_H",
		"E2" = "SETOTHERMODE_L",
		"E1" = "RDPHALF_1",
		"E0" = "SPNOOP",
		"F1" = "RDPHALF_2"
	}
	
	for cmd in commands:
		_commands[cmd] = commands[cmd]

##################
# Command Handlers
##################

func HANDLE_NOOP(model: N64Model, index: int) -> void: # 00
	super.HANDLE_NOOP(model, index) # is a command relocation

func HANDLE_VTX(model: N64Model, index: int) -> void: # 01
	var vtx_cnt  : int = model.get_display_command_bits(index, 12, 8)
	var vtx_pos  : int = model.get_display_command_byte(index, 3) + (vtx_cnt / 2)
	var seg_addr : int = model.get_display_command_bytes(index, 5, 3)
	
	for i in range(vtx_pos, vtx_pos + vtx_cnt):
		_vertex_list[i] = model.get_vertex(seg_addr)
		seg_addr += 0x10

func HANDLE_MODIFYVTX(model: N64Model, index: int) -> void: # 02
	pass

func HANDLE_CULLDL(model: N64Model, index: int) -> void: # 03
	super.HANDLE_CULLDL(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_BRANCH_Z(model: N64Model, index: int) -> void: # 04
	super.HANDLE_BRANCH_Z(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_TRI1(model: N64Model, index: int) -> void: # 05
	var v1 : int = model.get_display_command_byte(index, 1) / 0x2
	var v2 : int = model.get_display_command_byte(index, 2) / 0x2
	var v3 : int = model.get_display_command_byte(index, 3) / 0x2
	_mesh_renderer.draw_array(
		PackedVector3Array([ # vertices
			_vertex_list[v1].position * n64_vertex_scale,
			_vertex_list[v3].position * n64_vertex_scale,
			_vertex_list[v2].position * n64_vertex_scale
		]),
		PackedVector2Array([ # uvs
			_vertex_list[v1].uv,
			_vertex_list[v3].uv,
			_vertex_list[v2].uv
		]),
		PackedColorArray([   # colors
			_vertex_list[v1].color,
			_vertex_list[v3].color,
			_vertex_list[v2].color
		])
	)

func HANDLE_TRI2(model: N64Model, index: int) -> void: # 06
	super.HANDLE_TRI2(model, index) # is a command relocation

func HANDLE_QUAD(model: N64Model, index: int) -> void: # 07
	super.HANDLE_QUAD(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_ENDDL(model: N64Model, index: int) -> void: # DF
	super.HANDLE_ENDDL(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_DL(model: N64Model, index: int) -> void: # DE
	super.HANDLE_DL(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_LOAD_UCODE(model: N64Model, index: int) -> void: # DD
	super.HANDLE_LOAD_UCODE(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_MOVEMEM(model: N64Model, index: int) -> void: # DC
	super.HANDLE_MOVEMEM(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_MOVEWORD(model: N64Model, index: int) -> void: # DB
	super.HANDLE_MOVEWORD(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_MTX(model: N64Model, index: int) -> void: # DA
	super.HANDLE_MTX(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_GEOMETRYMODE(model: N64Model, index: int) -> void: # D9
	pass

func HANDLE_POPMTX(model: N64Model, index: int) -> void: # D8
	super.HANDLE_POPMTX(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_TEXTURE(model: N64Model, index: int) -> void: # D7
	super.HANDLE_TEXTURE(model, index) # is a command relocation

func HANDLE_DMA_IO(model: N64Model, index: int) -> void: # D6
	pass

func HANDLE_SPECIAL_1(model: N64Model, index: int) -> void: # D5
	pass

func HANDLE_SPECIAL_2(model: N64Model, index: int) -> void: # D4
	pass

func HANDLE_SPECIAL_3(model: N64Model, index: int) -> void: # D3
	pass

func HANDLE_SETOTHERMODE_H(model: N64Model, index: int) -> void: # E3
	super.HANDLE_SETOTHERMODE_H(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_SETOTHERMODE_L(model: N64Model, index: int) -> void: # E2
	super.HANDLE_SETOTHERMODE_L(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_RDPHALF_1(model: N64Model, index: int) -> void: # E1
	super.HANDLE_RDPHALF_1(model, index) # is a command relocation (Unconfirmed?)

func HANDLE_SPNOOP(model: N64Model, index: int) -> void: # E0
	super.HANDLE_SPNOOP(model, index) # is a command relocation

func HANDLE_RDPHALF_2(model: N64Model, index: int) -> void: # F1
	super.HANDLE_RDPHALF_2(model, index) # is a command relocation (Unconfirmed?)
