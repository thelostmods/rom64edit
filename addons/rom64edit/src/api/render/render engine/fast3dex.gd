@tool
class_name Fast3DEX
extends Fast3D

#####################
# Command Initializer
#####################

func _init_commands():
	super._init_commands()
	
	var commands : Dictionary = {
		"02" = "RESERVED0",
		"05" = "RESERVED1",
		"07" = "RESERVED2",
		"08" = "RESERVED3",
		"09" = "SPRITE2D_BASE",
		"AF" = "LOAD_UCODE",
		"B5" = "QUAD", # LINE3D (Only used in Line3D)
		"B1" = "TRI2",
		"B0" = "BRANCH_Z",
		"E4" = "TEXRECT"
	}
	
	for cmd in commands:
		_commands[cmd] = commands[cmd]

##################
# Command Handlers
##################

func HANDLE_RESERVED0(model: N64Model, index: int) -> void: # 02
	pass

func HANDLE_VTX(model: N64Model, index: int) -> void: # 04
	var vtx_pos  : int = model.get_display_command_byte(index, 1) / 2
	var vtx_cnt  : int = model.get_display_command_bits(index, 16, 6)
	var vtx_len  : int = model.get_display_command_bits(index, 22, 10)
	var seg_addr : int = model.get_display_command_bytes(index, 5, 3) / 0x10
	
	if vtx_cnt != ((vtx_len + 1) / 0x10):
		push_error("VTX: Parity check failure!")
	
	for i in range(vtx_pos, vtx_pos + vtx_cnt):
		_vertex_list[i] = model.get_vertex(seg_addr)
		seg_addr += 1

func HANDLE_RESERVED1(model: N64Model, index: int) -> void: # 05
	pass

func HANDLE_RESERVED2(model: N64Model, index: int) -> void: # 07
	pass

func HANDLE_RESERVED3(model: N64Model, index: int) -> void: # 08
	pass

func HANDLE_SPRITE2D_BASE(model: N64Model, index: int) -> void: # 09
	pass

func HANDLE_LOAD_UCODE(model: N64Model, index: int) -> void: # AF
	pass

func HANDLE_QUAD(model: N64Model, index: int) -> void: #B5
	pass

func HANDLE_TRI2(model: N64Model, index: int) -> void: # B1
	var v1 : int = model.get_display_command_byte(index, 1) / 0x2
	var v2 : int = model.get_display_command_byte(index, 2) / 0x2
	var v3 : int = model.get_display_command_byte(index, 3) / 0x2
	var v4 : int = model.get_display_command_byte(index, 5) / 0x2
	var v5 : int = model.get_display_command_byte(index, 6) / 0x2
	var v6 : int = model.get_display_command_byte(index, 7) / 0x2
	
	_mesh_renderer.draw_array(
		PackedVector3Array([ # vertices
			_vertex_list[v1].position * n64_vertex_scale,
			_vertex_list[v3].position * n64_vertex_scale,
			_vertex_list[v2].position * n64_vertex_scale,
			_vertex_list[v4].position * n64_vertex_scale,
			_vertex_list[v6].position * n64_vertex_scale,
			_vertex_list[v5].position * n64_vertex_scale
		]),
		PackedVector2Array([ # uvs
			_vertex_list[v1].uv,
			_vertex_list[v3].uv,
			_vertex_list[v2].uv,
			_vertex_list[v4].uv,
			_vertex_list[v6].uv,
			_vertex_list[v5].uv
		]),
		PackedColorArray([ # colors
			_vertex_list[v1].color,
			_vertex_list[v3].color,
			_vertex_list[v2].color,
			_vertex_list[v4].color,
			_vertex_list[v6].color,
			_vertex_list[v5].color
		])
	)

func HANDLE_BRANCH_Z(model: N64Model, index: int) -> void: # B0
	pass

func HANDLE_TEXRECT(model: N64Model, index: int) -> void: # E4
	pass
