@tool
class_name RenderEngine

#####################
# Protected variables
#####################

var _commands         : Dictionary
var _mesh_renderer    : MeshRenderer

var _palette          : PackedColorArray
var _vertex_list      : Array[N64Vertex]

var _geometry_mode    : int
var _cur_texture      : int
var _new_texture      : bool
var _tex_scale        : Vector2
var _st_scale         : Vector2

##################
# Global variables
##################

var log_commands     : bool
var n64_vertex_scale : float = 0.01

func _init():
	_init_commands()

func _init_commands(): pass

func render(model: N64Model) -> Mesh:
	# reinit the model generator
	_mesh_renderer = MeshRenderer.new()
	
	# reinit the vertex list
	_vertex_list.resize(0x64)
	for i in _vertex_list.size():
		_vertex_list[i] = N64Vertex.new()
	
	# invoke all commands
	for index in model.display_list_count():
		var cmd : String = model.get_display_command_name(index)
		
		# ensure command exists
		if not _commands.has(cmd):
			push_error("Command [%s] not found in render engine!" % cmd)
			continue
		
		if log_commands: print("%s: %s" % [str(index), _commands[cmd]])
		
		# try to call command handler
		var handler : String = "HANDLE_%s" % _commands[cmd]
		if not has_method(handler):
			push_error("Function [%s] not found in render engine!" % handler)
			continue
		
		call(handler, model, index)
	
	# return mesh instance
	return _mesh_renderer.render()
