class_name MeshRenderer

var _positions : PackedVector3Array
var _uvs       : PackedVector2Array
var _colors    : PackedColorArray

# add vertices to mesh.
func draw(position: Vector3, uv: Vector2, color: Color) -> void:
	_positions.append(position)
	_uvs.append(uv)
	_colors.append(color)

# add vertices to mesh.
func draw_array(
	position_list: PackedVector3Array,
	uv_list: PackedVector2Array,
	color_list: PackedColorArray
) -> void:
	_positions.append_array(position_list)
	_uvs.append_array(uv_list)
	_colors.append_array(color_list)

func render(material: Material = null) -> Mesh:
	# initialize the surface array.
	var surfaces := Array()
	surfaces.resize(ArrayMesh.ARRAY_MAX)
	surfaces[ArrayMesh.ARRAY_VERTEX] = _positions
	surfaces[ArrayMesh.ARRAY_TEX_UV] = _uvs
	surfaces[ArrayMesh.ARRAY_COLOR]  = _colors
	
	# add surfaces to mesh.
	var mesh := ArrayMesh.new()
	mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, surfaces)
	
	if material == null:
		material = load("res://addons/rom64edit/assets/materials/default.tres")
	
	mesh.surface_set_material(0, material)
	return mesh
