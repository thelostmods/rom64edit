@tool
class_name TextureCache

var data : PackedByteArray
var info : Array[TextureInfo]

func count() -> int:
	return info.size()

func set_info(index: int, offset: int, width: int, height: int) -> void:
	info[index].offset = offset
	info[index].width = width
	info[index].height = height

func find_index(offset: int) -> int:
	for i in info.size():
		var tmp : int = info[i].offset
		if tmp <= offset and offset <= tmp + 0x40:
			return i
	
	push_error("No texture data found on offset [%02X]" % offset)
	return -1

func _init(count: int) -> void:
	info.resize(count)
	
	for i in count:
		info[i] = TextureInfo.new()
