@tool
class_name N64Header

###############################################################
# Returns the cpu clock rate override (Value of 0 uses default)
###############################################################
static func get_clock_rate(arr: PackedByteArray) -> int:
	return BigEndian.arr_u32(arr, Hardware.header_clock_rate)

#############################
# Returns the program counter
#############################
static func get_program_counter(arr: PackedByteArray) -> int:
	return BigEndian.arr_u32(arr, Hardware.header_program_counter)

##################################
# Returns the first checksum value
##################################
static func get_checksum1(arr: PackedByteArray) -> int:
	return BigEndian.arr_u32(arr, Hardware.header_crc1)

###################################
# Returns the second checksum value
###################################
static func get_checksum2(arr: PackedByteArray) -> int:
	return BigEndian.arr_u32(arr, Hardware.header_crc2)

###############################
# Returns the games 'good-name'
###############################
static func get_game_name(arr: PackedByteArray) -> String:
	return Helper.c_string(arr, Hardware.header_name)

###############################################
# Returns the serial-number or game-id as short
###############################################
static func get_serial(arr: PackedByteArray) -> int:
	return BigEndian.arr_u16(arr, Hardware.header_game_id)

################################################
# Returns the serial-number or game-id as String
################################################
static func get_game_id(arr: PackedByteArray) -> String:
	var offset : int = Hardware.header_game_id
	return char(arr[offset]) + char(arr[offset + 1])

#################################
# Returns the country/region code
#################################
static func get_country(arr: PackedByteArray) -> String:
	return char(arr[Hardware.header_country_code])

#############################
# Returns the revision number
#############################
static func get_revision(arr: PackedByteArray) -> int:
	return arr[Hardware.header_revision]

#################################
# Returns the public build string
#################################
static func get_edition(arr: PackedByteArray) -> String:
	return get_country(arr) + str(get_revision(arr))

###########################
# Returns the release value
###########################
static func get_release_value(arr: PackedByteArray) -> int:
	return BigEndian.arr_u32(arr, arr[Hardware.header_release_pointer])
