# Rom64Edit

This is the official repository for the rom64edit development.

# What is Rom64Edit?

Rom64Edit is a plugin-extendable development suite for romhacking. It is a sibling project to CrossEmu to cover mod development in the rom side of things, as CrossEmu only intends ram hacking.

The goal of Rom64Edit is to make an all-in-one development environment that can be expanded to cover all areas of development for any given game allowing for actions such as decompression, recompression, file-dump, file-injection, data modification, scene translation, automation of producing patch files for your rom hack, and project organization (for developing multiple romhacks without collecting a messy environment).

# How does it all work?

All of this operates as a plugin to Godot Engine 4.0 which provides API for extra tool development and an interface for activating addon tools.

# Where do I get the engine/editor?

Godot Engine 4.0 is currently in beta/testing and is located at:
https://downloads.tuxfamily.org/godotengine/testing/4.0/

# How do I make a mod?

Todo: Fill this in.

# What do I need to know to begin modding?

Todo: Fill this in.

# How do I make a tool/plugin?

Todo: Fill this in.

# Is there any where I can get live-support?

Yes! Simply join the discord which can be accessed at:
https://discord.com/invite/EdH7jkh
